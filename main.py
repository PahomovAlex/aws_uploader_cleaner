import boto3
from bson import ObjectId
import motor.motor_asyncio
from asyncio import get_event_loop, wait
from os import getenv


async def find_in_photo(file_name, mongo_client):
    result = mongo_client.profile_db['photo'].find_one({"photo_content_id": file_name})
    if result:
        # print("found in photo")
        return True
    return False


async def find_in_greeting(file_name, mongo_client):
    result = mongo_client.profile_db['greeting'].find_one({"greeting_avatar_id": file_name})
    if result:
        # print("found in greeting")
        return True
    return False


async def find_in_event(file_name, mongo_client):
    result = mongo_client.event_db['event'].find_one({"$or": [{"event_image.avatar.id": file_name},
                                                              {"event_image.cover.id": file_name},
                                                              {"event_image.banner.id": file_name}
                                                              ]})
    if result:
        # print("found in event")
        return True
    return False


async def find_in_dialog_message(file_name, mongo_client):
    result = mongo_client.chat_db['dialog_message'].find_one({"$or": [{"message_content.data_id": file_name},
                                                                      {"message_content.preview_id": file_name}
                                                                      ]})
    if result:
        # print("found in dialog_message")
        return True
    return False


async def find_in_room_message(file_name, mongo_client):
    result = mongo_client.chat_db['room_message'].find_one({"$or": [{"message_content.data_id": file_name},
                                                                    {"message_content.preview_id": file_name}
                                                                    ]})
    if result:
        # print("found in room_message")
        return True
    return False


async def find_in_room(file_name, mongo_client):
    result = mongo_client.chat_db['room'].find_one({"photo_avatar_id": file_name})
    if result:
        # print("found in room")
        return True
    return False


async def find_file_in_other_collections(file_name, mongo_client):
    if await find_in_photo(file_name, mongo_client):
        return True
    elif await find_in_greeting(file_name, mongo_client):
        return True
    elif await find_in_event(file_name, mongo_client):
        return True
    elif await find_in_dialog_message(file_name, mongo_client):
        return True
    elif await find_in_room_message(file_name, mongo_client):
        return True
    elif await find_in_room(file_name, mongo_client):
        return True
    else:
        return False


async def find_file_in_upload(file_id, db):
    res = await db['upload'].find_one({"_id": ObjectId(file_id)})
    return res


async def delete_file_from_upload(file_id, db):
    res = await db['upload'].delete_one({"_id": ObjectId(file_id)})
    print("delete_upload ", file_id, res)
    return res


async def worker(bucket_name):
    client = motor.motor_asyncio.AsyncIOMotorClient(
        f"mongodb://{getenv('MONGO_USER')}:{getenv('MONGO_PASSWORD')}@{getenv('MONGO_HOST')}:{int(getenv('MONGO_PORT'))}/?authSource=admin&replicaSet={getenv('MONGO_REPLICA_SET')}")
    db = client.media_db

    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucket_name)

    all_objects = bucket.objects.all()
    for item in all_objects:
        file = await find_file_in_upload(str(item.key).rpartition('.')[0], db)
        if file:
            if not await find_file_in_other_collections(item.key, client):
                await delete_file_from_upload(file['_id'], db)
        else:
            result = item.delete()
            print("delete_AWS ", item.key, result)


if __name__ == '__main__':
    buckets = ['skylove-test', 'skylove-audio', 'skylove-file', 'skylove-image', 'skylove-video']
    # buckets = ['skylove-image']
    loop = get_event_loop()
    task = [worker(buckets[i]) for i in range(len(buckets))]
    loop.run_until_complete(wait(task))
    loop.close()
